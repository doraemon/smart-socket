package net.vinote.smart.socket.demo.http.application;

public enum URLPatternType {
	/** 完全匹配 */
	AllMatcher,
	/** 目录匹配 */
	CatalogMatcher,
	/** 扩展名匹配 */
	ExtensionMatcher,

}
